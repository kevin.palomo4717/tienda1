<h1>ESTA ES LA PAGINA DONDE VAMOS A CREAR LA BASE DE DATOS</h1>
<form class="" action="index.html" method="post">
  <div class="row">
      <div class="col-md-4 text-center">
        <label for="">ID de empleado</label>
        <br>
        <input type="text"
        placeholder="Ingrese el ID del empleado"
        class="form-control"
        name="IDempleado"
        value="">
      </div>
      <div class="col-md-4 text-center">
        <label for="">Nombre</label>
        <br>
        <input type="text"
        placeholder="Ingrese el Nombre del empleado"
        class="form-control"
        name="nom_emp"
        value="">
      </div>
      <div class="col-md-4 text-center">
        <label for="">Apellido</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido del empleado"
        class="form-control"
        name="ape_emp"
        value="">
      </div>
  </div>
  <br>
  <div class="row">
      <div class="col-md-6 text-center">
        <label for="">Genero</label>
        <br>
        <input type="text"
        placeholder="Ingrese el genero del empleado"
        class="form-control"
        name="genero_emp"
        value="">
      </div>
        <div class="col-md-6 text-center">
        <label for="">Celular</label>
        <br>
        <input type="number"
        placeholder="Ingrese el celular del empleado"
        class="form-control"
        name="celular_emp"
        value="">
      </div>
  </div>
<br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>&nbsp;
    <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">CANCELAR</a>

  </div>

  </div>

</form>

<br>
