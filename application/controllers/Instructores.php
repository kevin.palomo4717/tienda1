<?php
  class Instructores extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
    }

    public function index(){
      $this->load->view('header');
      $this->load->view('instructores/index');
      $this->load->view('footer');
    }
    public function pag1(){
      $this->load->view('header');
      $this->load->view('instructores/pag1');
      $this->load->view('footer');
    }
    public function pag2(){
      $this->load->view('header');
      $this->load->view('instructores/pag2');
      $this->load->view('footer');
    }
    public function pag3(){
      $this->load->view('header');
      $this->load->view('instructores/pag3');
      $this->load->view('footer');
    }
    public function pag4(){
      $this->load->view('header');
      $this->load->view('instructores/pag4');
      $this->load->view('footer');
    }
  }
